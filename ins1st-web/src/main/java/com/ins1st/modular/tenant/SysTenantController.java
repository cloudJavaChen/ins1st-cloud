package com.ins1st.modular.tenant;

import com.ins1st.api.SysTenantsApi;
import com.ins1st.base.R;
import com.ins1st.entity.SysTenants;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-22 15:40
 **/
@Controller
@RequestMapping(value = "/tenant")
public class SysTenantController {

    private static final String MODEL = "pages/tenant/";

    @Autowired
    private SysTenantsApi sysTenantsApi;

    /**
     * 主页
     *
     * @return
     */
    @RequestMapping(value = "/index")
    public String index() {
        return MODEL + "tenant_index.html";
    }


    /**
     * 分页
     *
     * @param sysTenants
     * @return
     */
    @RequestMapping(value = "/page")
    @ResponseBody
    public Object page(SysTenants sysTenants) {
        return sysTenantsApi.page(sysTenants);
    }


    /**
     * 更新
     *
     * @param sysTenants
     * @return
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SysTenants sysTenants) {
        R r = this.sysTenantsApi.insertOrUpdate(sysTenants);
        return r;
    }


    /**
     * 删除
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(String id) {
        R r = this.sysTenantsApi.remove(id);
        return r;
    }

    /**
     * 添加页
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/info")
    public String add(Model model, SysTenants sysTenants) {
        if (StringUtils.isNotBlank(sysTenants.getId())) {
            R<SysTenants> r = this.sysTenantsApi.selectOne(sysTenants);
            sysTenants = r.getData();
            model.addAttribute("sysTenants", sysTenants);
        }
        return MODEL + "tenant_info.html";
    }


    /**
     * 保存或更新
     *
     * @param sysTenants
     * @return
     */
    @RequestMapping(value = "/insertOrUpdate")
    @ResponseBody
    public Object insertOrUpdate(SysTenants sysTenants) {
        R r = this.sysTenantsApi.insertOrUpdate(sysTenants);
        return r;
    }
}
