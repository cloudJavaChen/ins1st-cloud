let treeGrid = null;
layui.use(['treeGrid', 'ax', 'table'], function () {

    treeGrid = layui.treeGrid;
    let tableId = "tableId";
    let $ax = layui.ax;

    treeGrid.render({
        id: tableId
        , elem: '#' + tableId
        , idField: 'id'
        , url: ctx + '/sys/menu/list'
        , cellMinWidth: 100
        , treeId: 'id'
        , treeUpId: 'pId'
        , size: "sm"
        , treeShowName: 'name'
        , toolbar: true
        , toolbar: "#toolbarTpl"
        , cols: [[
            {field: 'id', width: 100, title: 'id'}
            , {field: 'name', width: 200, title: '菜单名称'}
            , {field: 'url', width: 200, title: '菜单地址'}
            , {field: 'role', width: 200, title: '菜单权限'}
            , {
                field: 'isMenu', width: 200, title: '是否菜单', templet: function (d) {
                    if (d.isMenu == '0') {
                        return "不是";
                    }
                    return "是";
                }
            }
            , {field: 'sort', width: 300, title: '排序'}
            , {title: "操作", align: "center", fixed: "right", templet: "#operationTpl"}

        ]]
        , page: false
    });


    treeGrid.on("tool(tableFilter)", function (obj) {
        let event = obj.event;
        let data = obj.data;
        switch (event) {
            case "del":
                admin.confirm("是否删除该菜单(删除父菜单会一起删除子菜单)?", function () {
                    var ax = new $ax(ctx + "/sys/menu/delete", function (result) {
                        admin.success(result.message);
                        treeGrid.reload("tableId");
                    });
                    ax.set("id", obj.data.id);
                    ax.start();
                });
                break;
            case "edit":
                admin.open("编辑菜单", ctx + "/sys/menu/edit?id=" + data.id, "90%", "90%", null, function () {
                    treeGrid.reload("tableId");
                });
                break;
        }
    });
});

function add() {
    admin.open("添加菜单", ctx + "/sys/menu/add", "90%", "90%", null, function () {
        treeGrid.reload("tableId");
    });
}