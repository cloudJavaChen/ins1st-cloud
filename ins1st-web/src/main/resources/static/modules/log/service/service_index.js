layui.use(['table', 'ax', 'form'], function () {
    let table = layui.table;
    let $ax = layui.ax;
    let form = layui.form;

    table.render({
        elem: '#tableId',
        url: ctx + '/log/service/page',
        limit: 20,
        size: "sm",
        page: true,
        toolbar: true,
        toolbar: "#toolbarTpl",
        cols: [[
            {type: "radio", fixed: "left"},
            {field: "id", title: "ID", width: 100, sort: true},
            {field: "serviceName", title: "服务名"},
            {field: "logName", title: "业务名称"},
            {field: "methodName", title: "方法名称"},
            {field: "className", title: "类名称"},
            {field: "methodParams", title: "方法参数"},
            {field: "logTime", title: "记录时间"},
        ]]
    });
});