package com.ins1st.base;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-13 13:39
 **/
public class PageQuery implements Serializable {

    @TableField(exist = false)
    @JSONField(serialize = false)
    private int limit;
    @TableField(exist = false)
    @JSONField(serialize = false)
    private int page;

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
