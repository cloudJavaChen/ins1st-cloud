package com.ins1st.exception;

import feign.Response;
import feign.Util;
import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-18 15:07
 **/
@Configuration
public class ExceptionErrorDecoder implements ErrorDecoder {

    @Override
    public Exception decode(String s, Response response) {
        if (response.body() != null) {
            if (response.status() == 500) {
                //封装返回业务异常
                throw new BussinessException("系统错误,请联系管理员");
            }
        }
        return null;
    }
}
