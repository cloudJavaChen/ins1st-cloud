package com.ins1st.api;

import com.ins1st.entity.JavaMail;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@FeignClient(name = "ins1st-message", contextId = "MessageApi")
public interface MessageApi {


    /**
     * 发送邮件
     *
     * @param javaMail
     * @return
     */
    @RequestMapping(value = "/message/sendEmail", method = RequestMethod.POST)
    boolean sendEmail(@RequestBody JavaMail javaMail);


    /**
     * 发送短信
     * @param tpId
     * @param mobile
     * @param map
     * @return
     */
    @RequestMapping(value = "/message/sendSms", method = RequestMethod.POST)
    boolean sendSms(@RequestParam("tpId") String tpId, @RequestParam("mobile") String mobile, @RequestBody Map<String, String> map);
}
