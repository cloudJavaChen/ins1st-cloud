package com.ins1st.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ins1st.entity.SysLoginLog;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-20 13:51
 **/
public interface SysLoginLogMapper extends BaseMapper<SysLoginLog> {
}
