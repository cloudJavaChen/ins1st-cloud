package com.ins1st;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@EnableTransactionManagement
public class Ins1stSystemApplication {

    private static final Logger log = LoggerFactory.getLogger(Ins1stSystemApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(Ins1stSystemApplication.class, args);
        log.info(Ins1stSystemApplication.class.getSimpleName() + " is start success");
    }

}
