package com.ins1st.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ins1st.api.SysServiceLogApi;
import com.ins1st.base.PageResult;
import com.ins1st.base.R;
import com.ins1st.entity.SysServiceLog;
import com.ins1st.mapper.SysServiceLogMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-20 16:18
 **/
@RestController
@Transactional
public class SysServiceLogApiImpl extends ServiceImpl<SysServiceLogMapper, SysServiceLog> implements SysServiceLogApi {

    @Override
    public PageResult page(SysServiceLog sysServiceLog) {
        QueryWrapper qw = new QueryWrapper();
        qw.like(StringUtils.isNotBlank(sysServiceLog.getLogName()), "log_name", sysServiceLog.getLogName());
        qw.like(StringUtils.isNotBlank(sysServiceLog.getServiceName()), "service_name", sysServiceLog.getServiceName());
        IPage<SysServiceLog> page = this.baseMapper.selectPage(new Page(sysServiceLog.getPage(), sysServiceLog.getLimit()), qw);
        return R.page(page);
    }

    @Override
    public R insert(SysServiceLog sysServiceLog) {
        this.save(sysServiceLog);
        return R.success();
    }
}
