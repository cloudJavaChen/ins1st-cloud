package com.ins1st.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ins1st.api.SysMenuApi;
import com.ins1st.base.R;
import com.ins1st.entity.MenuNavs;
import com.ins1st.entity.SysMenu;
import com.ins1st.entity.SysRoleMenu;
import com.ins1st.exception.BussinessException;
import com.ins1st.mapper.SysMenuMapper;
import com.ins1st.mapper.SysRoleMenuMapper;
import com.ins1st.tree.Ztree;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-11 11:26
 **/
@RestController
@Transactional
public class SysMenuApiImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements SysMenuApi {

    @Autowired
    private SysRoleMenuMapper sysRoleMenuMapper;


    @Override
    public R queryIndexMenus(String userId){
        List<MenuNavs> menuNavs = new ArrayList<>();
        List<SysMenu> parents = this.baseMapper.queryMenusByUserId(userId);
        for (SysMenu sysMenu : parents) {
            if (sysMenu.getpId().equals("0")) {
                int childs = this.baseMapper.selectList(new QueryWrapper<SysMenu>().eq("p_id", sysMenu.getId())).size();
                if (childs > 0) {
                    MenuNavs nv = new MenuNavs();
                    nv.setTitle(sysMenu.getName());
                    nv.setHref(sysMenu.getUrl());
                    nv.setIcon(sysMenu.getIcon());
                    this.handlerMenus(sysMenu, nv);
                    menuNavs.add(nv);
                } else {
                    if (sysMenu.getIsMenu().equals("1")) {
                        MenuNavs nv = new MenuNavs();
                        nv.setTitle(sysMenu.getName());
                        nv.setHref(sysMenu.getUrl());
                        nv.setIcon(sysMenu.getIcon());
                        if (sysMenu.getId().equals(" == 33")) {
                            nv.setCheck(true);
                            nv.setSpread(true);
                        }
                        menuNavs.add(nv);
                    }
                }
            }
        }
        return R.success(menuNavs);
    }

    @Override
    public R<List<Ztree>> selectZtree(String roleId) {
        List<SysMenu> sysMenuList = this.baseMapper.selectList(null);
        List<SysRoleMenu> roleMenuList = this.sysRoleMenuMapper.selectList(new QueryWrapper<SysRoleMenu>().eq("role_id", roleId));
        List<Ztree> ztreeList = new ArrayList<>();
        for (SysMenu sysMenu : sysMenuList) {
            Ztree ztree = new Ztree();
            ztree.setId(sysMenu.getId());
            ztree.setpId(sysMenu.getpId());
            ztree.setName(sysMenu.getName());
            for (SysRoleMenu sysRoleMenu : roleMenuList) {
                if (sysRoleMenu.getMenuId().equals(sysMenu.getId())) {
                    ztree.setChecked(true);
                }
            }
            ztreeList.add(ztree);
        }
        return R.success(ztreeList);
    }

    @Override
    public R saveRoleMenus(String roleId, String menuIds) {
        this.sysRoleMenuMapper.delete(new QueryWrapper<SysRoleMenu>().eq("role_id", roleId));
        String[] ids = menuIds.split(",");
        for (String id : ids) {
            SysRoleMenu sysRoleMenu = new SysRoleMenu();
            sysRoleMenu.setRoleId(roleId);
            sysRoleMenu.setMenuId(id);
            this.sysRoleMenuMapper.insert(sysRoleMenu);
        }
        return R.success("操作成功");
    }

    private void handlerMenus(SysMenu sysMenu, MenuNavs nv) {
        List<MenuNavs> menuNavsList = new ArrayList<>();
        List<SysMenu> childrens = this.baseMapper.selectList(new QueryWrapper<SysMenu>().eq("p_id", sysMenu.getId()));
        for (SysMenu child : childrens) {
            if (child.getIsMenu().equals("1")) {
                MenuNavs menuNavs = new MenuNavs();
                menuNavs.setTitle(child.getName());
                menuNavs.setHref(child.getUrl());
                menuNavs.setIcon(child.getIcon());
                menuNavsList.add(menuNavs);
                int size = this.baseMapper.selectList(new QueryWrapper<SysMenu>().eq("p_id", child.getId())).size();
                if (size > 0) {
                    this.handlerMenus(child, menuNavs);
                }
                nv.setChildren(menuNavsList);
            }
        }
    }

    @Override
    public R<List<SysMenu>> list(SysMenu sysMenu) {
        QueryWrapper qw = new QueryWrapper();
        qw.eq(StringUtils.isNotBlank(sysMenu.getIsMenu()), "is_menu", sysMenu.getIsMenu());
        qw.orderByAsc("sort");
        List<SysMenu> sysMenuList = this.baseMapper.selectList(qw);
        R<List<SysMenu>> r = new R<>();
        r.setData(sysMenuList);
        return r;
    }

    @Override
    public R del(String id) {
        this.baseMapper.deleteById(id);
        List<SysMenu> children = this.baseMapper.selectList(new QueryWrapper<SysMenu>().eq("p_id", id));
        if (children != null && children.size() > 0) {
            for (SysMenu child : children) {
                this.baseMapper.deleteById(child.getId());
                handlerDelete(child);
            }
        }
        return R.success("删除成功");
    }

    private void handlerDelete(SysMenu sysMenu) {
        List<SysMenu> children = this.baseMapper.selectList(new QueryWrapper<SysMenu>().eq("p_id", sysMenu.getId()));
        if (children != null && children.size() > 0) {
            for (SysMenu child : children) {
                this.baseMapper.deleteById(child.getId());
                handlerDelete(child);
            }
        }
    }

    @Override
    public R<SysMenu> selectOne(String id) {
        SysMenu sysMenu = this.baseMapper.selectById(id);
        if ("0".equals(sysMenu.getpId())) {
            sysMenu.setpName("顶级菜单");
        } else {
            SysMenu parent = this.baseMapper.selectById(sysMenu.getpId());
            sysMenu.setpName(parent.getName());
        }
        R<SysMenu> r = new R<>();
        r.setData(sysMenu);
        return r;
    }

    @Override
    public R insertOrUpdate(SysMenu sysMenu) {
        if (StringUtils.isNotBlank(sysMenu.getId())) {
            this.baseMapper.updateById(sysMenu);
        } else {
            this.baseMapper.insert(sysMenu);
        }
        return R.success();
    }

    @Override
    public R<List<SysMenu>> queryMenusByUserId(String id) {
        return R.success(this.baseMapper.queryMenusByUserId(id));
    }
}
