package com.ins1st.api;

import com.ins1st.base.PageResult;
import com.ins1st.base.R;
import com.ins1st.entity.SysRole;
import com.ins1st.tree.Ztree;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-14 13:09
 **/
@FeignClient(name = "ins1st-system", contextId = "SysRoleApi")
public interface SysRoleApi {

    @RequestMapping(value = "/role/page", method = RequestMethod.POST)
    PageResult page(@RequestBody SysRole sysRole);

    @RequestMapping(value = "/role/insertOrUpdate", method = RequestMethod.POST)
    R insertOrUpdate(@RequestBody SysRole sysRole);

    @RequestMapping(value = "/role/remove", method = RequestMethod.POST)
    R remove(@RequestParam("id") String id);

    @RequestMapping(value = "/role/selectOne", method = RequestMethod.POST)
    R<SysRole> selectOne(@RequestParam("id") String id);

    @RequestMapping(value = "/role/selectZtree", method = RequestMethod.POST)
    R<List<Ztree>> selectZtree(@RequestParam("userId") String userId);

    @RequestMapping(value = "/role/saveUserRoles", method = RequestMethod.POST)
    R saveUserRoles(@RequestParam("userId") String userId, @RequestParam("roleIds") String roleIds);
}
