package com.ins1st.api;

import com.ins1st.base.R;
import com.ins1st.entity.SysUpload;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-21 15:49
 **/
@FeignClient(name = "ins1st-system", contextId = "SysUploadApi")
public interface SysUploadApi {

    @RequestMapping(value = "/upload/save", method = RequestMethod.POST)
    R<SysUpload> insert(@RequestBody SysUpload sysUpload);
}
