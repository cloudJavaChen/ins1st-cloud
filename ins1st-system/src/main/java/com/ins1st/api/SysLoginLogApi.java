package com.ins1st.api;

import com.ins1st.base.PageResult;
import com.ins1st.base.R;
import com.ins1st.entity.SysLoginLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-20 13:49
 **/
@FeignClient(name = "ins1st-system", contextId = "SysLoginLogApi")
public interface SysLoginLogApi {

    @RequestMapping(value = "/log/login/page", method = RequestMethod.POST)
    PageResult page(@RequestBody SysLoginLog sysLoginLog);

    @RequestMapping(value = "/log/login/insert")
    R insert(@RequestBody SysLoginLog sysLoginLog);
}
