package com.ins1st.api;

import com.ins1st.base.PageResult;
import com.ins1st.base.R;
import com.ins1st.entity.SysServiceLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-20 16:17
 **/
@FeignClient(name = "ins1st-system", contextId = "SysServiceLogApi")
public interface SysServiceLogApi {

    @RequestMapping(value = "/log/service/page", method = RequestMethod.POST)
    PageResult page(@RequestBody SysServiceLog sysServiceLog);

    @RequestMapping(value = "/log/service/insert")
    R insert(@RequestBody SysServiceLog sysServiceLog);
}
